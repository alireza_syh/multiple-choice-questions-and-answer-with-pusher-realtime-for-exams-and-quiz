@extends('layouts.app')

@section('head')

    <script src="https://js.pusher.com/6.0/pusher.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        Pusher.logToConsole = true;

        var pusher = new Pusher('0ef7c844516487f0545b', {
            cluster: 'ap1'
        });

        var channel = pusher.subscribe('my-channel');
        channel.bind('mainevent', function(data) {
            //alert(JSON.stringify(data));
            $( ".container2" ).prepend( "<div class=\"row\">\n" +
                "        <div class=\"col-md-8 col-md-offset-2\">\n" +
                "            <div class=\"panel panel-default\">\n" +
                "                <div class=\"panel-heading\"></div>\n" +
                "\n" +
                "                <div style=\"direction: rtl;text-align: right\" class=\"panel-body\">\n" +data.question.toString().replace(/\r\n|\n|\r/g, '<br />')+
                "                      \n" +
                "                </div>\n" +
                "<div class=\"panel-footer\" style=\"margin: 0 auto;direction: rtl\">\n" +
                "                        <center>\n" +
                "                        <form method=\"get\" action=\"votecdkjfdldsadsadas\">" +
                "<button name=\"option\" value=\""+ data.options[0] + "\" style=\"width: 140px;height: 65px\"> الف<br></button>" +
                "<button name=\"option\" value=\""+ data.options[1] + "\" style=\"width: 140px;height: 65px\"> ب<br></button>" +
                "<button name=\"option\" value=\""+ data.options[2] + "\" style=\"width: 140px;height: 65px\"> ج<br></button>" +
                "<button name=\"option\" value=\""+ data.options[3] + "\" style=\"width: 140px;height: 65px\"> د<br></button> " +
                "<input name=\"question\" value=\""+ data.questionid + "\" type=\"hidden\"/>\n" +
                " <input name=\"_token\" value=\"{{ csrf_token() }}\" type=\"hidden\"" +
                "                        </form>\n" +
                "                        </center>\n" +
                "                    </div>" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n " );
        });
    </script>
@endsection


@section('content')
<div class="container2">
    @foreach($questions as $q)
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $q->id }}</div>

                    <div style="direction: rtl;text-align: right" class="panel-body">
                        {{ $q->question }}<br><br>
                        @foreach($q->questionoption as $m)
                            {{ $m->option->option }}<br><br>
                            @endforeach
                    </div>
                    <div class="panel-footer" style="margin: 0 auto;direction: rtl">
                        <center>
                        <form method="post" action="{{ route('vote') }}">
                            {{ csrf_field() }}
                            <?php $counter=1  ?>
                            @foreach($q->questionoption as $m)
                                @if($counter==1)
                                <button name="option" value="{{ $m->option->id }}" style="width: 140px;height: 65px"> الف<br>{{ $m->option->vote }}</button>
                                    @elseif($counter==2)
                                        <button name="option" value="{{ $m->option->id }}" style="width: 140px;height: 65px"> ب<br>{{ $m->option->vote }}</button>
                                    @elseif($counter==3)
                                        <button name="option" value="{{ $m->option->id }}" style="width: 140px;height: 65px"> ج<br>{{ $m->option->vote }}</button>
                                    @elseif($counter==4)
                                        <button name="option" value="{{ $m->option->id }}" style="width: 140px;height: 65px"> د<br>{{ $m->option->vote }}</button>
                                    @endif
                                <?php $counter++ ?>
                            @endforeach
                            <input name="question" value="{{ $q->id }}" type="hidden"/>
                        </form>
                        </center>
                    </div>
                </div>
            </div>
        </div>

        @endforeach

</div>
@endsection
