@extends('layouts.app')

@section('head')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <style>
        .textform{
            text-align: right;
            float: right;
        }
        .btnform{
            width: 220px;
            height: 50px;
            font-size: large;
        }
        .taform{
            width: 100%;
            text-align: right;
            direction: rtl;
        }

    </style>

@endsection


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">سوال جدید</div>

                <div class="panel-body">
                    <form method="post" action="{{ route('ques') }}">
                        {{ csrf_field() }}
                        <label class="textform">سوال</label>
                        <textarea class="taform" rows="10" cols="20" name="quest" placeholder="كدام جمله درباره smurf attack صحيح است؟

الف) مربوط به پروتكل ICMP مي باشد؛ يك حمله DoS است. مشابه TCP SYN flood است.

ب) مربوط به پروتكل ICMP مي باشد؛ يك حمله DDoS است. مشابه TCP SYN flood است.

ج) مربوط به پروتكل ICMP مي باشد؛ يك حمله DDoS است. مشابه fraggle است.

د) مربوط به پروتكل ICMP مي باشد؛ يك حمله DoS است. مشابه fraggle است.???" ></textarea>
                    <button type="submit" name="submit" class="btn-success btnform">ثبت</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
