<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question',
    ];
    public function questionoption()
    {
        return $this->hasMany('App\QuestionOption');
    }
    public function userquestion()
    {
        return $this->hasMany('App\UserQuestion');
    }
}
