<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserQuestion extends Model
{
    protected $table="user_question";
    protected $fillable = [
        'user_id', 'question_id',
    ];
    public function question()
    {
        return $this->belongsTo('App\Question','question_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
