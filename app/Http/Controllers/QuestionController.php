<?php

namespace App\Http\Controllers;

use App\Events\mainevent;
use App\Option;
use App\Question;
use App\QuestionOption;
use App\UserOption;
use App\UserQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showpage(){
        return view('sendQ');
    }
    public function submitquest(Request $request){
        $question=$request->quest;
        $question=trim($question,"\x00..\x1F");
        $parts=explode("\n",$question);
        $is=Question::where('question','like','%'.$parts[0].'%')->first();
        if($is !== null && $is !== ""){
            return redirect('home');
            die();
        }
        $is=Question::get();
        $flag=false;
        foreach ($is as $m){
            $test=strstr($parts[0],$m->text);
            if ($test == null && $test == "" && !isset($test)){
                $flag=true;
                break;
            }
        }
        if ($flag==true){
            return redirect('home');echo $test;
            die();
        }

        $questionsave=Question::create([
           'question'=>$parts[0],
        ]);
        $userquest=UserQuestion::create([
            'user_id'=>Auth::user()->id,
            'question_id'=>$questionsave->id,
        ]);
        $optionss=[];
        if (isset($parts[8])) {
            $count=0;
            for ($i = 2; $i <= 8; $i += 2) {
                $optionsave = Option::create([
                    'option' => $parts[$i],
                ]);
                $optionss[$count]=$optionsave->id;
                $optionquestion = QuestionOption::create([
                    'option_id' => $optionsave->id,
                    'question_id' => $questionsave->id,
                ]);
                $count++;
            }
        }else{
            $count=0;
            for ($i = 2; $i <= 5; $i++) {
                $optionsave = Option::create([
                    'option' => $parts[$i],
                ]);
                $optionss[$count]=$optionsave->id;
                $optionquestion = QuestionOption::create([
                    'option_id' => $optionsave->id,
                    'question_id' => $questionsave->id,
                ]);
                $count++;
            }
        }


        event(new mainevent($question,$optionss,$questionsave->id));

        return redirect('home');

    }

    public function vote(Request $request){
        $question_id=$request->question;
        $option_id=$request->option;
         $options=QuestionOption::where('question_id',$question_id)->get();
         foreach ($options as $o){
             $finduser=UserOption::where('option_id',$o->option_id)->where('user_id',Auth::user()->id)->first();
             if(isset($finduser->id)){
                 return redirect('home');
                 die();
             }
         }

         $useroption=UserOption::create([
            'user_id'=> Auth::user()->id,
             'option_id'=>$option_id,
         ]);
         $op=Option::where('id',$option_id)->first();
         $finalcount=$op->vote + 1;
        $opupdate=Option::find($option_id);
        $opupdate->vote=$finalcount;
        $opupdate->save();

        return redirect('home');
    }

    public function vote2(Request $request){
        $question_id=$request->question;
        $option_id=$request->option;
        $options=QuestionOption::where('question_id',$question_id)->get();
        foreach ($options as $o){
            $finduser=UserOption::where('option_id',$o->option_id)->where('user_id',Auth::user()->id)->first();
            if(isset($finduser->id)){
                return redirect('home');
                die();
            }
        }

        $useroption=UserOption::create([
            'user_id'=> Auth::user()->id,
            'option_id'=>$option_id,
        ]);
        $op=Option::where('id',$option_id)->first();
        $finalcount=$op->vote + 1;
        $opupdate=Option::find($option_id);
        $opupdate->vote=$finalcount;
        $opupdate->save();

        return redirect('home');
    }
}
