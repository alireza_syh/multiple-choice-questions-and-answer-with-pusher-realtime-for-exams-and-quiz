<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionOption extends Model
{
    protected $table="question_option";
    protected $fillable = [
        'question_id', 'option_id',
    ];
    public function question()
    {
        return $this->belongsTo('App\Question','question_id');
    }
    public function option()
    {
        return $this->belongsTo('App\Option','option_id');
    }
}
