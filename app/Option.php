<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = [
        'option',
    ];
    public function useroption()
    {
        return $this->hasMany('App\UserOption');
    }
    public function questionoption()
    {
        return $this->hasMany('App\QuestionOption');
    }
}
