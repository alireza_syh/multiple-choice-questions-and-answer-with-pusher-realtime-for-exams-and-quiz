<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOption extends Model
{
    protected $table="user_option";
    protected $fillable = [
        'user_id', 'option_id',
    ];
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function option()
    {
        return $this->belongsTo('App\Option','option_id');
    }
}
